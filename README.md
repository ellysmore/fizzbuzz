## APPLICATION NAME ##
Fizz Buzz

## VERSION ##
1.0.0

## DESCRIPTION ##
A native Android app that lists the numbers 1 to 100 in a ListView, but for multiples of 3, display “Fizz” and for multiples of 5 print “Buzz”.  For numbers that are multiples of 3 and 5 print “FizzBuzz.”
User also has the option to input their own multiples and ranges.

## INSTALLATION ##
To install, you can build the project yourself by downloading the file.

## CREDITS ##
Developer: Eileen Yau

## KNOWN BUGS ##

## CHANGELOG ##