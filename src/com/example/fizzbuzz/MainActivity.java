package com.example.fizzbuzz;

import java.util.ArrayList;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends Activity {

	
	public static int firstMultiple;
	public static int secondMultiple;
	public static int minRange;
	public static int maxRange;
	
	ArrayList<String> itemList = new ArrayList<String>();
	ArrayAdapter<String> itemsAdapter;

	ListView theView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
		//Grab intent from previous activity that started this activity. 
		Intent menuIntent = getIntent();
		if(menuIntent!=null){
			firstMultiple = menuIntent.getIntExtra(MenuScreen.PASS_FIRST_MULTIPLE, 3);
			secondMultiple = menuIntent.getIntExtra(MenuScreen.PASS_SECOND_MULTIPLE, 5);
			minRange = menuIntent.getIntExtra(MenuScreen.PASS_MIN_RANGE, 1);
			maxRange = menuIntent.getIntExtra(MenuScreen.PASS_MAX_RANGE, 100);
		}
		
		theView = (ListView) findViewById(R.id.listView);
		
		fizzbuzz(firstMultiple, secondMultiple, minRange, maxRange);
		
		itemsAdapter = new ArrayAdapter<String>(getApplicationContext(),
				R.layout.custom_list_item, itemList);

		theView.setAdapter(itemsAdapter);

	}

	
	/**
	 * This is a method will take in four parameters and loops to see if any of the value between
	 * the minRange and maxRange has any multiples of the firstMultiple and/or secondMultiple.
	 * If so, the item on the list view will show either "Fizz", "Buzz" or "FizzBuzz".  
	 */
	private void fizzbuzz(int firstMultiple, int secondMultiple, int minRange, int maxRange) { 
		for (int i = minRange; i <= maxRange; i++) {
			
			if (i % firstMultiple == 0 && i % secondMultiple == 0) {
				itemList.add("FizzBuzz");
			} else if (i % firstMultiple == 0) {
				itemList.add("Fizz");
			} else if (i % secondMultiple == 0) {
				itemList.add("Buzz");
			}else{
				itemList.add(String.valueOf(i));
			}

		}

	}

}
