package com.example.fizzbuzz;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MenuScreen extends Activity implements View.OnClickListener {

	EditText editText_firstMultiple;
	EditText editText_secondMultiple;
	EditText editText_minRange;
	EditText editText_maxRange;

	TextView textview_errormsg;
	Button button_start;

	String string_firstMultiple, string_secondMultiple, string_minRange,
			string_maxRange;

	// Default Values
	final int default_firstMultiple = 3;
	final int default_secondMultiple = 5;
	final int default_minRange = 1;
	final int default_maxRange = 100;

	int firstMultiple, secondMultiple, minRange, maxRange;

	public static final String PASS_FIRST_MULTIPLE = "com.example.fizzbuzz.firstMultiple";
	public static final String PASS_SECOND_MULTIPLE = "com.example.fizzbuzz.secondMultiple";
	public static final String PASS_MIN_RANGE = "com.example.fizzbuzz.minRange";
	public static final String PASS_MAX_RANGE = "com.example.fizzbuzz.maxRange";

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu_screen);

		editText_firstMultiple = (EditText) findViewById(R.id.editText_firstMultiple);
		editText_secondMultiple = (EditText) findViewById(R.id.editText_secondMultiple);
		editText_minRange = (EditText) findViewById(R.id.editText_minRange);
		editText_maxRange = (EditText) findViewById(R.id.editText_maxRange);
		textview_errormsg = (TextView) findViewById(R.id.textView_errormsg);

		button_start = (Button) findViewById(R.id.button_start);

		button_start.setOnClickListener(this);
	}

	/**
	 * This method checks to determine whether the inputs in the textfields are
	 * valid. If not, the textview under the button will show an error msg.
	 */
	private boolean checkFormInputs() {
		// Grab text from EditText views and store them as strings
		string_minRange = editText_minRange.getText().toString();
		string_maxRange = editText_maxRange.getText().toString();

		/*
		 * Grab minimum and maximum range. If they are empty they are given
		 * default values.
		 */
		if (string_minRange.isEmpty())
			minRange = default_minRange;
		else
			minRange = Integer.parseInt(string_minRange);

		System.out.println("MinRange: " + minRange);

		if (string_maxRange.isEmpty())
			maxRange = default_maxRange;
		else
			maxRange = Integer.parseInt(string_maxRange);

		System.out.println("MaxRange: " + maxRange);

		// Check the rest of the EditText view
		if (minRange < maxRange) {

			/*
			 * Grab first multiple and second multiple. If they are empty they
			 * are given default values.
			 */
			string_firstMultiple = editText_firstMultiple.getText().toString();
			string_secondMultiple = editText_secondMultiple.getText()
					.toString();

			if (string_firstMultiple.isEmpty())
				firstMultiple = default_firstMultiple;
			else
				firstMultiple = Integer.parseInt(string_firstMultiple);

			System.out.println("firstMultiple: " + firstMultiple);

			if (string_secondMultiple.isEmpty())
				secondMultiple = default_secondMultiple;
			else
				secondMultiple = Integer.parseInt(string_secondMultiple);

			System.out.println("secondMultiple: " + secondMultiple);
			return true;

		} else {
			textview_errormsg.setText("Error: MinRange exceeds MaxRange.\nPlease input again.");
			return false;
		}
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		textview_errormsg.setText("");
	}
	

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.button_start:

			// If inputs are valid, proceed to make the intent object, else,
			// show user error
			if (checkFormInputs()) {

				Intent listViewIntent = new Intent(this, MainActivity.class);

				// Storing values into the intent object that will be pass to
				// the next started activity
				listViewIntent.putExtra(PASS_FIRST_MULTIPLE, firstMultiple);
				listViewIntent.putExtra(PASS_SECOND_MULTIPLE, secondMultiple);
				listViewIntent.putExtra(PASS_MIN_RANGE, minRange);
				listViewIntent.putExtra(PASS_MAX_RANGE, maxRange);

				this.startActivity(listViewIntent);

			}
			break;

		default:

		}
	}
}
